name = ['allejahre', 'stillenacht', 'kinderleinkommet', 'weihnachtsbaume']
gedichte = ['Alle Jahre wieder \nkommt das Christuskind ',
            'Stille Nacht! Heilige Nacht! \nAlles schläft; einsam wacht',
            'Ihr Kinderlein, kommet, o kommet doch all! \nZur Krippe her kommet in Betlehems Stall',
            'Am Weihnachtsbaum die Lichter brennen ...']
l = [0,0,0,0]

def beenden():
    for i in range [4]:
        if l[i] == 1:
            return False
    return True

while True:

    frage = input('Geben Sie bitte ein von den Gedichtsnamen ein: allejahre, stillenacht, kinderleinkommet, weihnachtsbaume: \n')

    if frage == 'allejahre':
        gedichte_tmp = gedichte[0]
        print(gedichte_tmp)
        l[0] = 1
        if beenden == True:
            break
        else:
            continue

    if frage == 'stillenacht':
        gedichte_tmp = gedichte[1]
        print(gedichte_tmp)
        l[1] = 1
        if beenden == True:
            break
        else:
            continue

    if frage == 'kinderleinkommet':
        gedichte_tmp = gedichte[2]
        print(gedichte_tmp)
        l[2] = 1
        if beenden == True:
            break
        else:
            continue

    if frage == 'weihnachtsbaume':
        gedichte_tmp = gedichte[3]
        print(gedichte_tmp)
        l[3] = 1
        if beenden == True:
            break
        else:
            continue

    if frage == 'ohmmm':
        break



