name = ['allejahre', 'stillenacht', 'kinderleinkommet', 'weihnachtsbaume']
gedichte = ['Alle Jahre wieder \nkommt das Christuskind ',
            'Stille Nacht! Heilige Nacht! \nAlles schläft; einsam wacht',
            'Ihr Kinderlein, kommet, o kommet doch all! \nZur Krippe her kommet in Betlehems Stall',
            'Am Weihnachtsbaum die Lichter brennen ...']
print('''Es ist Zeit für die Weihnachtsstimmung. Bitte wählen Sie Ihr eigenes Lied aus den Index. 
Wenn jedes der Lieder mindestens einmal ausgegeben oder "ohmmm" eingegeben wird, endet sich die Weihnachtsstimmung.''')
print("\n")
count = set()
while True:
    print("""********* Index für Gedichte *********\n
         0: allejahre\n
         1: stillenacht'\n
         2: kinderleinkommet\n
         3: weihnachtsbaume\n""")
    if len(count) == 4:
        print("Jedes der Lieder wurde mindestens einmal ausgegeben. Weihnachtsstimmung beendet.")
        exit()
    option = input("Bitte wählen Sie ein Index aus. ")
    if option == "ohmmm":
        print("Weihnachtsstimmung beendet.")
        exit()
    if option in ["0", "1", "2", "3"]:
        print(gedichte[int(option)])
        count.add(option)
    else:
        print("Bitte geben Sie 0, 1, 2, 3 oder ohmmm ein.")
