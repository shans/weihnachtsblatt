import random as rand

while True:
    eingabe = input("Was möchten Sie tun?(5 Würfel werfen /q für Beenden) ")
    match eingabe:
        case "5 Würfel werfen":
            numbers = [rand.randint(1, 6) for i in range(5)]
            for num in numbers:
                print(num, end=" ")
            print("\n")
        case "$":
            numbers = [rand.randint(1, 6)] * 5
            for num in numbers:
                print(num, end=" ")
            print("\n")
        case "q":
            exit()
        case other:
            print('Bitte, geben Sie "5 Würfel werfen" oder "q" ein.')
