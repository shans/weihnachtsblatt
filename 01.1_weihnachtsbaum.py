def weihnachts_hanoi():
    """
    Implementierung einer Weihnachtsversion der Türme von Hanoi


    """

    # Die Schichten liegen als Tupel aus ihrer größe (int) und ihrer darstellung (str) vor,
    # um sie leichter vergleichen zu können
    SCHICHT1 = (1, "    /\\    ")
    SCHICHT2 = (2, "   /^^\\   ")
    SCHICHT3 = (3, "  /^^^^\\  ")
    SCHICHT4 = (4, " /^^^^^^\\ ")
    SCHICHT5 = (5, "/^^^^^^^^\\")

    # Zu Beginn ist der Baum im Keller untergebracht und soll über das Treppenhaus in das Wohnzimmer
    # verfrachtet werden.
    keller = [SCHICHT5, SCHICHT4, SCHICHT3, SCHICHT2, SCHICHT1]
    treppenhaus = []
    wohnzimmer = []

    while baum_verschieben(keller, treppenhaus, wohnzimmer):
        print_bäume(keller, treppenhaus, wohnzimmer)
    print_bäume(keller, treppenhaus, wohnzimmer)


def baum_verschieben(keller, treppenhaus, wohnzimmer):
    """
    Interaktion mit Nutzer und Verschieben der einzelnen Schichten

    Parameter:
    keller ((int, str)): Der Keller Stapel
    treppenhaus ((int, str)): Der Treppenhaus Stapel
    wohnzimmer ((int, str)): Der Wohnzimmer Stapel

    Return:
    (bool) True, wenn noch weitere Schichten herum getragen werden müssen, False sonst.

    """
    eingabe_von = input("Nehmen von Keller[k] Treppenhaus[t] oder Wohnzimmer[w]?")
    while True:
        if eingabe_von == "k" and keller:
            scheibe = keller.pop()
            break
        elif eingabe_von == "t" and treppenhaus:
            scheibe = treppenhaus.pop()
            break
        elif eingabe_von == "w" and wohnzimmer:
            scheibe = wohnzimmer.pop()
            break
        else:
            print("Ungültige Eingabe!")
    print("Aufgenommen:", scheibe[1])
    print_bäume(keller, treppenhaus, wohnzimmer)

    while True:
        eingabe_nach = input("Ablegen auf Keller[k] Treppenhaus[t] oder Wohnzimmer[w]?")
        if eingabe_nach == "k" and zug_gültig(eingabe_nach, scheibe, keller, treppenhaus, wohnzimmer):
            keller.append(scheibe)
            break
        elif eingabe_nach == "t" and zug_gültig(eingabe_nach, scheibe, keller, treppenhaus, wohnzimmer):
            treppenhaus.append(scheibe)
            break
        elif eingabe_nach == "w" and zug_gültig(eingabe_nach, scheibe, keller, treppenhaus, wohnzimmer):
            wohnzimmer.append(scheibe)
            break
        else:
            print("Fehler")

    return not fertig(keller, treppenhaus, wohnzimmer)


def zug_gültig(zug, scheibe, keller, treppenhaus, wohnzimmer):
    """
    Diese Funktion gibt Auskunft darüber ob ein gegebener Zug gültig ist.
    Eine größere Schicht darf nicht auf eine kleinere gelegt werden.

    Parameter:
    zug (str): "k" für auf Kellerstapel lege, "t" für auf Treppenhausstapel legen,
        "w" für auf Wohnzimmerstapel legen
    scheibe ((int, str)): Die Scheibe, die gerade getragen wird
    keller ([(int,str)]): Der Kellerstapel
    treppenhaus ([(int,str)]): Treppenhausstapel
    wohnzimmer ([(int, str)]): Wohnzimmerstapel

    Return:
    (bool) True falls die Scheibe auf diesen Stapel gelegt werden darf, False sonst
    """

    if zug == 'k':
        if keller == []:
            return True
        else:
            k_tmp = keller.pop()
            if k_tmp[0] < scheibe[0]:
                keller.append(k_tmp)
                return False
            else:
                keller.append(k_tmp)
                return True

    if zug == 't':
        if treppenhaus == []:
            return True
        else:
            t_tmp = treppenhaus.pop()
            if t_tmp[0] < scheibe[0]:
                treppenhaus.append(t_tmp)
                return False
            else:
                treppenhaus.append(t_tmp)
                return True

    if zug == 'w':
        if wohnzimmer == []:
            return True
        else:
            w_tmp = wohnzimmer.pop()
            if w_tmp[0] < scheibe[0]:
                wohnzimmer.append(w_tmp)
                return False
            else:
                wohnzimmer.append(w_tmp)
                return True


def fertig(keller, treppenhaus, wohnzimmer):
    """
    Diese Funktion soll prüfen ob die Falmilie fertig ist

    Parameter:
    keller ([(int,str)]): Der Kellerstapel
    treppenhaus ([(int,str)]): Treppenhausstapel
    wohnzimmer ([(int, str)]): Wohnzimmerstapel

    Return:
    (bool) True, falls der komplette Baum im Wohnzimmer steht, False sonst
    """

    if len(wohnzimmer) == 5:
        return True
    else:
        return False


def print_bäume(keller, treppenhaus, wohnzimmer):
    """
    Gibt die Bäume auf dem Bildschirm aus

    Parameter:
    keller (int, str): Der Keller Stapel
    treppenhaus (int, str): Der Treppenhaus Stapel
    wohnzimmer (int, str): Der Wohnzimmer Stapel
    """
    GRÜN = '\033[92m'
    SCHWARZ = '\033[0m'
    print(GRÜN, end="")
    for i in range(5, -1, -1):
        if i in range(len(keller)):
            print(keller[i][1] + " ", end="")
        else:
            print("           ", end="")
        if i in range(len(treppenhaus)):
            print(treppenhaus[i][1] + " ", end="")
        else:
            print("           ", end="")
        if i in range(len(wohnzimmer)):
            print(wohnzimmer[i][1])
        else:
            print("          ")
    print("    ||         ||         ||    " + SCHWARZ)
    print("  keller  treppenhaus wohnzimmer")

weihnachts_hanoi()