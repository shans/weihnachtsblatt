import random as rand

# mithilfe eines Zufallsgenerators 5000 zufällige Würfelsummen berechnen und
# angeben wie hoch der Anteil an gewüfelten geraden und ungeraden Augensummen ist.

anzahl = 2
seiten = 6
versuch = 5000

augensumme_anzahl = [0] * (anzahl * seiten + 1)

# Würfeln alle Versuche
for _ in range(versuch):
    augensumme = 0

    # Bestimme die Summe aller geworfenen Würfel
    for _ in range(anzahl):
        augensumme += rand.randint(1, seiten)
    # Passe die gefundene anzahl an würfen an
    augensumme_anzahl[augensumme] += 1

# Gebe die Augensummen mit ihrem prozentuellen Anteil an vorgekommenen Würfen an
procent = [(i, augensumme_anzahl[i] / versuch) for i in range(2, len(augensumme_anzahl))]
even = [pro for i, pro in procent if i % 2 == 0]
odd = [pro for i, pro in procent if i % 2 != 0]

print(f"Gerade: {sum(even)}")
print(f"Ungerade: {sum(odd)}")
