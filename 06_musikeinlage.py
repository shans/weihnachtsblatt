# 0 <= Rand < 1.0, randomBoolean() gibt nie "False" zurück.
import random


def random_boolean():
    return random.choice([True, False])
