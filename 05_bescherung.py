def initial_board():
    # ein zunächst leeres Spielfeld wird erstellt
    spalte = ['_'] * 7
    spielfeld = []
    for _ in range(7):
        spielfeld.append(spalte[:])
    # die Liste Spielstand überwacht, in welcher Spalte wie viele Steine bereits eingeworfen wurden,
    # indem sie die nächste freie Stelle der Spalte n an der Position n speichert
    spielstand = [0] * 7

    # eine Liste mit den Namen und den Spielsteinen der Spieler
    spieler_liste = [('rot', 'R'), ('gelb', 'G')]

    # der Index des Spielers aus der Spielerliste, der gerade am Zug ist
    am_zug = 0

    spiel_fertig = False
    return spielfeld, spielstand, spieler_liste, am_zug, spiel_fertig


def spielfeld_anzeigen():
    # diese Funktion gibt das Spielfeld auf dem Bildschirm aus. Anmerkung: ein genaues Verständnis von der exakten
    # Funktionsweise dieser Funktion ist nicht erforderlich
    spielfeld_fertig = [reihe[:] for reihe in spielfeld]
    for i in range(7):
        for j in range(7):
            spielfeld_fertig[6 - j][i] = spielfeld[i][j]
    for reihe in spielfeld_fertig:
        print(reihe)
    print('  1    2    3    4    5    6    7')


def spielzug(spieler, gewaehlte_spalte):
    """
    Trägt einen Spielzug ins Spielfeld ein

    Parameter:
    spieler (str, str): Der Spieler, dessen Spielstein ins Spielfeld geworfen wird
    gewaehlteSpalte (int): Die Spalte des Spielfelds, in der der Spieler seinen Spielstein wirft
    """
    freies_feld = spielstand[gewaehlte_spalte - 1]
    spielfeld[gewaehlte_spalte - 1][freies_feld] = spieler[1]
    spielstand[gewaehlte_spalte - 1] += 1


def check_win(spieler, board):
    game = False
    # Horizontal checker
    for j in range(0, 6):
        for i in range(3, 7):
            if board[j][i] == board[j][i - 1] == board[j][i - 2] == board[j][i - 3] == spieler:
                game = True
            else:
                continue
    # Vertical checker
    for i in range(0, 7):
        for j in range(3, 6):
            if board[j][i] == board[j - 1][i] == board[j - 2][i] == board[j - 3][i] == spieler:
                game = True
            else:
                continue
    # Diagonal checker
    for i in range(0, 4):
        for j in range(0, 3):
            if board[j][i] == board[j + 1][i + 1] == board[j + 2][i + 2] == board[j + 3][i + 3] == spieler or \
                    board[j + 3][i] == board[j + 2][i + 1] == board[j + 1][i + 2] == board[j][i + 3] == spieler:
                game = True
            else:
                continue
    if game is True:
        print(spieler + ' wins!')
    if all([board[i][j] for i in range(0, 7) for j in range(0, 7)]) == "G" or \
            all([board[i][j] for i in range(0, 7) for j in range(0, 7)]) == "R":
        game = True
        print("Ein Unentschieden!")
    return game


if __name__ == "__main__":
    spielfeld, spielstand, spieler_liste, am_zug, spiel_fertig = initial_board()
    spielfeld_anzeigen()
    while not spiel_fertig:
        eingabe = int(input('Spieler ' + spieler_liste[am_zug][0] + ' ist am Zug. Wählen die Spalte: '))
        if eingabe == 'ende':
            print('Auf Wiedersehen')
            spiel_fertig = True
        elif eingabe == 'neu':
            spielfeld, spielstand, spieler_liste, am_zug, spiel_fertig = initial_board()
            print('Neues Spiel gestartet')
            spielfeld_anzeigen()
        elif eingabe not in [1, 2, 3, 4, 5, 6, 7]:
            # Hier werden ungültige Eingaben abgefangen
            print('Ungültige Eingabe, bitte eine Zahl zwischen 1 und 7 eingeben')
        else:
            spielzug(spieler_liste[am_zug], eingabe)
            spielfeld_anzeigen()
            spiel_fertig = check_win(spieler_liste[am_zug][1], spielfeld)
            if spiel_fertig is True:
                break
            am_zug = 1 - am_zug
            spiel_fertig = check_win(spieler_liste[am_zug][1], spielfeld)
    print("Spiel vorbei!")
    exit()

