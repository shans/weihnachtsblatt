import random

bewohner = {'Dagobert': 74, 'Ingeborg': 77, 'Gertrude': 41, 'Dirk': 45, 'Timmy': 9, 'Eva': 19, 'Anton': 14}
KINDERALTER = range(0, 18)
# die Liste der möglichen Geschenke, sie darf auch gerne noch erweitert/angepasst werden
geschenke = ['Spielzeug', 'Socken', 'Kniffelspiel', 'Brettspiel']


def geschenk_hinlegen():
    geschenkberg = [(p, random.choice(geschenke)) for p in bewohner.keys()]
    return geschenkberg


def geschenke_anschauen(person):
    if bewohner[person] >= 18:
        print(f"{person} hat die geschenke geschaut.")
    else:
        print(f"{person}, nicht wühle den Geschenkberg vor der Bescherung durch!")


def bescherung_einleiten(geschenkberg):
    for p, g in geschenkberg:
        print(f"{p} hat {g} bekommen!")


def geschenkberg_app():
    bescherung = False
    geschenkberg = geschenk_hinlegen()
    while not bescherung:
        names = list(bewohner.keys())
        geschenke_anschauen(random.choice(names))
        bescherung = int(input("Ist Heiligabend schon vorbei?(1 für Ja/0 für Nein) "))
    bescherung_einleiten(geschenkberg)


if __name__ == "__main__":
    geschenkberg_app()
