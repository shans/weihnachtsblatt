import random as rand

betreff = rand.choice(["die Vorlesung besuchen", "die Konferenz beteiligen", "zu den Terminen gehen",
                       "am Turnier teilnehmen"])
grund = rand.choice(["mein Konto wurde auf einem unbekannten Gerät eingeloggt und ich muss dem nachgehen",
                     "ich bin von der Steppdecke entführt worden und versuche herauszufinden, wie ich mich retten kann",
                     "es ist ein schöner Tag und ich sollte meinen Träumen nachjagen",
                     "der Aufzug ist kaputt und ich wohne in einem Stockwerk, das zu hoch ist, um hinunter zu kommen.",
                     "meine oberen Augenlider sind in meine unteren Augenlider verliebt und ich kann nichts tun, "
                     "um das zu verhindern."])

print(f"Entschuldigung. Ich kann nicht {betreff}, denn {grund}.")
